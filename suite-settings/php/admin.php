<?php

$directories = glob( CITSUITE__PLUGIN_DIR . '*', GLOB_ONLYDIR );
$directoryNames = [];

?>

<main id="cps-settings" class="wrap">

	<h1>Citadel Plugin Suite Settings</h1>

	<form method="post" action="options.php">

		<?php wp_nonce_field('update-options'); ?>

		<table class="form-table">


		<?php

		foreach ($directories as $directory) {

			$dir = basename( $directory );
			$cleanDir = str_replace("-", " ", $dir);

			if ( 'suite-settings' !== $dir ) {

				if ( is_network_admin() ) {

					$option = 'network-' . $dir . '-plugin';

					array_push($directoryNames, $option);

				} else {

					$option = $dir . '-plugin';

					array_push($directoryNames, $option);

				}
				

		?>

			<tr valign="top"><th scope="row"><?php echo $cleanDir; ?></th>

			<td><input type="checkbox" name="<?php echo $dir; ?>" value="1" <?php checked( 1, get_option( $option ), true ); ?> /></td>

			<?php var_dump(get_option($option)); ?>

		<?php

			}

		}

		?>

		</table>

		<input type="hidden" name="action" value="update" />

		<?php $list = implode(',', $directoryNames); ?>

		<input type="hidden" name="page_options" value="<?php echo $list; ?>" />

		<p class="submit">
			<input type="submit" class="button-primary" value="<?php _e( 'Save Changes' ); ?>" />
		</p>

	</form>

</main>

